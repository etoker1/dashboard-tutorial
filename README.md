# Dashboard Tutorial

For HL staff to create customized dashboards for personal use. Follow the instructions in the two documents to customize. To upload and publish, use your public directory on the Share platform.

Kit:
* index.html
* custom.css
* .htaccess

Steps:
1. Download the three files
2. In your SHARE account dashboard, go to FILE DIRECTORY
3. In your PUBLIC_HTML folder, create a new folder. Name it anything you like, but do not include spaces (e.g. "learning-object-library" or "mfa-dashboard")
4. Upload the file "INDEX.HTML"
5. Create a new folder in the dashboard called "css" and upload "CUSTOM.CSS" into the folder.
6. Upload the .HTACCESS file into the main dashboard folder. 
7. Customize the INDEX.HTML file 


If you've ended up here for an unknown reason [click here](https://etoker1.gitlab.io/dashboard-tutorial/) 
